#include <iostream>

int
main()
{
    int a, b, c;
    
    std::cout << "Enter three different integers: ";
    std::cin >> a >> b >> c;
    
    std::cout << "The amount is " << a + b + c << "\n";
    std::cout << "The average is " << (a + b + c) / 3 << "\n";
    std::cout << "The product is equal to " << a * b * c << "\n";
    
    if(a < b && a < c) {
        std::cout << "The least is " << a << "\n";
    } else if(b < a && b < c) {
        std::cout << "The least is " << b << "\n";
    } else {
        std::cout << "The least is " << c << "\n";
    }

    if(a > b && a > c) {
        std::cout << "The greatest is " << a << "\n";
    } else if(b > a && b > c) {
        std::cout << "The greatest is " << b << "\n";
    } else {
        std::cout << "The greatest is " << c << "\n";
    }

    return 0;
}
