/// Printing greeting
/// Author: Artak Yenokyan
#include <iostream>

int
main()
{
    std::cout << "Hello World!\n"; /// Printing command
    return 0;
}

