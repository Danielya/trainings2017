/// print the circle's diameter, circumference and area

#include <iostream> /// allows program to perform input and output

int
main()
{
    int r;
    std::cout << "Type radius of a circle: \n";
    std::cin >> r; /// the radius of a circle 
    if (r < 0) {
        std::cout << "The radius of a circle cannot be minus" << std::endl;
        return 1;
    }
    std::cout << "Circle's diameter is " << r * 2 << std::endl;
    std::cout << "Circumference is " << 2 * 3.14159 * r << std::endl;
    std::cout << "Area is " << 3.14159 * r * r << std::endl;

    return 0;
}
