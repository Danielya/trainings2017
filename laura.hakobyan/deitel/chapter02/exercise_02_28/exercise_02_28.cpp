/// prints the digits separated from one another by three spaces each
#include <iostream>
 
int
main() 
{ 
    int fiveDigitNumber;
    std::cout << "Enter five digit number: ";
    std::cin >> fiveDigitNumber;

    if (fiveDigitNumber < 10000) {
        std::cout << "Error 1: It isn't 5-digit number." << std::endl;
        return 1;
    }
    if (fiveDigitNumber > 99999) {
        std::cout << "Error 1: It isn't 5-digit number." << std::endl;
        return 1;
    }

    int digit1 = fiveDigitNumber / 10000;
    int digit2 = fiveDigitNumber % 10000 / 1000;
    int digit3 = fiveDigitNumber % 1000 / 100;
    int digit4 = fiveDigitNumber % 100 / 10;
    int digit5 = fiveDigitNumber % 10;
    std::cout << digit1 << "   " << digit2 << "   " << digit3 << "   " << digit4 << "   " << digit5 << std::endl;

    return 0;
}

