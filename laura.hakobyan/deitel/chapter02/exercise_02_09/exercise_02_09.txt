a. Print the message "Enter two numbers". -- std::cout << "Enter two numbers: ";
b. Assign the product of variables b and c to variable a. -- a = b * c;
c. State that a program performs a payroll calculation -- /// program performs a payroll calculation.
d. Input three integer values from the keyboard into integer variables a, b and c. -- std::cin >> a >> b >> c;
